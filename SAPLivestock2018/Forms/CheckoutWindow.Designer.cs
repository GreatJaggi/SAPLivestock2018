﻿namespace SAPLivestock2018.Forms
{
    partial class CheckoutWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.WindowPane = new System.Windows.Forms.Panel();
            this.cashDiscNUD = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.amountPayingNUD = new System.Windows.Forms.NumericUpDown();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.changeTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.customerNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.totalAmountDueTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.AcceptCheckout = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.OrderListDataGridView = new System.Windows.Forms.DataGridView();
            this.orderQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sIIL2018DataSet = new SAPLivestock2018.SIIL2018DataSet();
            this.WindowPane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cashDiscNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountPayingNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderListDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // WindowPane
            // 
            this.WindowPane.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WindowPane.BackColor = System.Drawing.SystemColors.ControlLight;
            this.WindowPane.Controls.Add(this.cashDiscNUD);
            this.WindowPane.Controls.Add(this.label7);
            this.WindowPane.Controls.Add(this.amountPayingNUD);
            this.WindowPane.Controls.Add(this.dateTimePicker1);
            this.WindowPane.Controls.Add(this.label1);
            this.WindowPane.Controls.Add(this.changeTextBox);
            this.WindowPane.Controls.Add(this.label2);
            this.WindowPane.Controls.Add(this.customerNameTextBox);
            this.WindowPane.Controls.Add(this.label4);
            this.WindowPane.Controls.Add(this.totalAmountDueTextBox);
            this.WindowPane.Controls.Add(this.label6);
            this.WindowPane.Controls.Add(this.AcceptCheckout);
            this.WindowPane.Controls.Add(this.ClearBtn);
            this.WindowPane.Controls.Add(this.label5);
            this.WindowPane.Controls.Add(this.label3);
            this.WindowPane.Controls.Add(this.OrderListDataGridView);
            this.WindowPane.Location = new System.Drawing.Point(1, 0);
            this.WindowPane.Name = "WindowPane";
            this.WindowPane.Size = new System.Drawing.Size(984, 561);
            this.WindowPane.TabIndex = 1;
            // 
            // cashDiscNUD
            // 
            this.cashDiscNUD.DecimalPlaces = 2;
            this.cashDiscNUD.Location = new System.Drawing.Point(851, 418);
            this.cashDiscNUD.Margin = new System.Windows.Forms.Padding(2);
            this.cashDiscNUD.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.cashDiscNUD.Name = "cashDiscNUD";
            this.cashDiscNUD.Size = new System.Drawing.Size(98, 20);
            this.cashDiscNUD.TabIndex = 34;
            this.cashDiscNUD.ThousandsSeparator = true;
            this.cashDiscNUD.ValueChanged += new System.EventHandler(this.cashDiscNUD_ValueChanged);
            this.cashDiscNUD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cashDiscNUD_KeyDown);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(752, 419);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Cash Discount";
            // 
            // amountPayingNUD
            // 
            this.amountPayingNUD.DecimalPlaces = 2;
            this.amountPayingNUD.Location = new System.Drawing.Point(851, 395);
            this.amountPayingNUD.Margin = new System.Windows.Forms.Padding(2);
            this.amountPayingNUD.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.amountPayingNUD.Name = "amountPayingNUD";
            this.amountPayingNUD.Size = new System.Drawing.Size(98, 20);
            this.amountPayingNUD.TabIndex = 32;
            this.amountPayingNUD.ThousandsSeparator = true;
            this.amountPayingNUD.ValueChanged += new System.EventHandler(this.amountPayingNUD_ValueChanged);
            this.amountPayingNUD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.amountPayingNUD_KeyDown);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(506, 415);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(399, 421);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Date of Transaction";
            // 
            // changeTextBox
            // 
            this.changeTextBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.changeTextBox.Location = new System.Drawing.Point(851, 442);
            this.changeTextBox.Name = "changeTextBox";
            this.changeTextBox.ReadOnly = true;
            this.changeTextBox.Size = new System.Drawing.Size(98, 20);
            this.changeTextBox.TabIndex = 31;
            this.changeTextBox.Text = "0.00";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(399, 389);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Customer Name";
            // 
            // customerNameTextBox
            // 
            this.customerNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customerNameTextBox.Location = new System.Drawing.Point(506, 386);
            this.customerNameTextBox.Name = "customerNameTextBox";
            this.customerNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.customerNameTextBox.TabIndex = 7;
            this.customerNameTextBox.TextChanged += new System.EventHandler(this.customerNameTextBox_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(783, 445);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Change";
            // 
            // totalAmountDueTextBox
            // 
            this.totalAmountDueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.totalAmountDueTextBox.Location = new System.Drawing.Point(851, 371);
            this.totalAmountDueTextBox.Name = "totalAmountDueTextBox";
            this.totalAmountDueTextBox.ReadOnly = true;
            this.totalAmountDueTextBox.Size = new System.Drawing.Size(98, 20);
            this.totalAmountDueTextBox.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(734, 375);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Total Amount Due";
            // 
            // AcceptCheckout
            // 
            this.AcceptCheckout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AcceptCheckout.Location = new System.Drawing.Point(853, 506);
            this.AcceptCheckout.Name = "AcceptCheckout";
            this.AcceptCheckout.Size = new System.Drawing.Size(96, 23);
            this.AcceptCheckout.TabIndex = 16;
            this.AcceptCheckout.Text = "Accept";
            this.AcceptCheckout.UseVisualStyleBackColor = true;
            this.AcceptCheckout.Click += new System.EventHandler(this.AcceptCheckout_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearBtn.Location = new System.Drawing.Point(772, 506);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(75, 23);
            this.ClearBtn.TabIndex = 15;
            this.ClearBtn.Text = "Cancel";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Checkout";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(749, 396);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Amount Paying";
            // 
            // OrderListDataGridView
            // 
            this.OrderListDataGridView.AllowUserToAddRows = false;
            this.OrderListDataGridView.AllowUserToDeleteRows = false;
            this.OrderListDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OrderListDataGridView.AutoGenerateColumns = false;
            this.OrderListDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.OrderListDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.OrderListDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderListDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderQty,
            this.ItemName,
            this.price,
            this.discount,
            this.orderidDataGridViewTextBoxColumn});
            this.OrderListDataGridView.DataSource = this.orderBindingSource;
            this.OrderListDataGridView.Location = new System.Drawing.Point(15, 25);
            this.OrderListDataGridView.Name = "OrderListDataGridView";
            this.OrderListDataGridView.ReadOnly = true;
            this.OrderListDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OrderListDataGridView.Size = new System.Drawing.Size(956, 340);
            this.OrderListDataGridView.TabIndex = 12;
            // 
            // orderQty
            // 
            this.orderQty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.orderQty.FillWeight = 58.98871F;
            this.orderQty.HeaderText = "QTY";
            this.orderQty.Name = "orderQty";
            this.orderQty.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ItemName.FillWeight = 164.8692F;
            this.ItemName.HeaderText = "Item Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // price
            // 
            this.price.HeaderText = "Price";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // discount
            // 
            this.discount.HeaderText = "Discount (%)";
            this.discount.Name = "discount";
            this.discount.ReadOnly = true;
            // 
            // orderidDataGridViewTextBoxColumn
            // 
            this.orderidDataGridViewTextBoxColumn.DataPropertyName = "order_id";
            this.orderidDataGridViewTextBoxColumn.HeaderText = "order_id";
            this.orderidDataGridViewTextBoxColumn.Name = "orderidDataGridViewTextBoxColumn";
            this.orderidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // orderBindingSource
            // 
            this.orderBindingSource.DataMember = "Order";
            this.orderBindingSource.DataSource = this.sIIL2018DataSet;
            // 
            // sIIL2018DataSet
            // 
            this.sIIL2018DataSet.DataSetName = "SIIL2018DataSet";
            this.sIIL2018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CheckoutWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.WindowPane);
            this.Name = "CheckoutWindow";
            this.Text = "CheckoutWindow";
            this.Load += new System.EventHandler(this.CheckoutWindow_Load);
            this.WindowPane.ResumeLayout(false);
            this.WindowPane.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cashDiscNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.amountPayingNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrderListDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel WindowPane;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox customerNameTextBox;
        private System.Windows.Forms.TextBox totalAmountDueTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button AcceptCheckout;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView OrderListDataGridView;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private SIIL2018DataSet sIIL2018DataSet;
        private System.Windows.Forms.BindingSource orderBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn discount;
        private System.Windows.Forms.NumericUpDown amountPayingNUD;
        private System.Windows.Forms.TextBox changeTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderquantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountDataGridViewTextBoxColumn;
        private System.Windows.Forms.NumericUpDown cashDiscNUD;
        private System.Windows.Forms.Label label7;
    }
}