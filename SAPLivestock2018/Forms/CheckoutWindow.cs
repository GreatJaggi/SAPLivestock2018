﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace SAPLivestock2018.Forms
{
    public partial class CheckoutWindow : Form
    {

        Transaction transaction;

        bool HasCheckoutProblem = false;
        bool IsCreditTransaction = false;
        public CheckoutWindow()
        {
            InitializeComponent();
        }

        private void CheckoutWindow_Load(object sender, EventArgs e)
        {}

        public void InitiateCheckoutLocal()
        {
            OrderListDataGridView.Columns.Clear();
            OrderListDataGridView.AutoGenerateColumns = true;
            OrderListDataGridView.DataSource = transaction.GenerateDataTable();
            OrderListDataGridView.Refresh();

            totalAmountDueTextBox.Text = transaction.GetTotalAmountDue().ToString();
        }

        public void SetTransaction(Transaction t)
        {
            transaction = t;
        }

        public Transaction GetTransaction()
        {
            return transaction;
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            //Cancel Checkout/Transaction
            MainWindow mainWindow = (MainWindow)this.ParentForm;
            try
            {
                mainWindow.CheckoutInterrupt(true);
            }
            catch (Exception)
            {
                MessageBox.Show("Error: Could not find MainWindow!");
                throw;
            }
        }
        
        private void amountPayingNUD_ValueChanged(object sender, EventArgs e)
        {
            int val;
            if (!int.TryParse(amountPayingNUD.Value.ToString(), out val))
                amountPayingNUD.Value = 0;

            double totalAmountDue = double.Parse(totalAmountDueTextBox.Text);
            double amountPaying = double.Parse(amountPayingNUD.Value.ToString());
            transaction.SetTotalAmountDue(totalAmountDue);
            transaction.SetAmountPaying(amountPaying);
            double change = transaction.GetChange();
            changeTextBox.Text = change.ToString();
        }

        public bool PerformFieldCheck()
        {
            // check if amount paying value is valid
            if ((double)amountPayingNUD.Value < double.Parse(totalAmountDueTextBox.Text) && transaction.GetCustomerName() == "")
            {
                MessageBox.Show(
                    "Note: If this section has a zero (0) or less value this will be a credit transaction and will require a customer name. Are you sure?",
                    "Credit Transaction");
                MessageBox.Show("Your negative change will be your credit transaction. \nPlease enter a customer name!", "Warning");
                changeTextBox.Text = transaction.GetChange().ToString();
                return false;
            }

            if((double)cashDiscNUD.Value > 0 && transaction.GetCustomerName() == "")
            {
                MessageBox.Show("Cash Discount operation requires a customer name!", "Invalid Input");
                return false;
            }

            return true;
        }
        private void AcceptCheckout_Click(object sender, EventArgs e)
        {
            if (amountPayingNUD.Text == "" || cashDiscNUD.Text == "") {
                MessageBox.Show("Please fill out some details!");
                return;
            }
                

            transaction.SetTransactionDateTime(dateTimePicker1.Value);

            if (PerformFieldCheck() == false)
                return;
                
            // 1. Save transaction data
            MessageBox.Show("Transaction Details \n" +
                "Date/Time: " + transaction.GetTransactionDateTime().ToString() + "\n" +
                "Total Amount Due: " + transaction.GetTotalAmountDue().ToString() + "\n" +
                "Amount Paying: " + transaction.GetAmountPaying().ToString() + "\n" +
                "Change: " + transaction.GetChange().ToString());

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            //compute Transaciton_ID
            cmd.Parameters.Add("tDateTime", SqlDbType.DateTime).Value = transaction.GetTransactionDateTime();
            cmd.Parameters.Add("totalAmountDue", SqlDbType.Money).Value = transaction.GetTotalAmountDue();
            cmd.Parameters.Add("amountPaying", SqlDbType.Money).Value = transaction.GetAmountPaying();
            cmd.Parameters.Add("change", SqlDbType.Money).Value = transaction.GetChange();
            cmd.Parameters.Add("cashDisc", SqlDbType.Money).Value = transaction.GetCashDiscount();
            cmd.Parameters.Add("customerName", SqlDbType.VarChar).Value = transaction.GetCustomerName();

            cmd.CommandText =
                "INSERT INTO [dbo].[Transaction] (customer_name, transaction_datetime, total_amount, amount_paying, change, cash_discount) "
                + "VALUES (@customerName, @tDateTime, @totalAmountDue, @amountPaying, @change, @cashDisc)";

            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            // 1.5 Update orders and put item ID
            /*
             * IDEA: Get the item on orders datagridview upon check out
             * and put each of them inside an order with transaction_id tagged with them
             *
             */
             
            Int32 transactionID = 0;
            string sql = "SELECT TOP 1 * FROM [dbo].[Transaction] ORDER BY transaction_id DESC";

            cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                transactionID = (Int32)cmd.ExecuteScalar();
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            List<string> orderNameList = transaction.GetOrderItemNameList();
            List<int> orderQtyList = transaction.GetOrderItemQtyList();
            List<double> orderPriceList = transaction.GetOrderItemPriceList();
            string orderNames = "";

            // print out transaction item details
            orderNames = "DESC\t\tQTY\t\tPRICE";
            for (int i = 0; i < orderNameList.Count; i++)
            {
                orderNames = string.Concat(orderNames, "\n" + orderNameList[i] + "\t\t" + orderQtyList[i] + "\t\t" + orderPriceList[i]);
            }

            // Insert into order table
            for (int i = 0; i < orderNameList.Count; i++)
            {
                cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandType = System.Data.CommandType.Text;

                cmd.Parameters.Add("itemDesc", SqlDbType.VarChar).Value = orderNameList[i];
                cmd.Parameters.Add("itemQty", SqlDbType.Int).Value = orderQtyList[i];
                cmd.Parameters.Add("itemPrice", SqlDbType.Money).Value = orderPriceList[i];
                cmd.Parameters.Add("transactionID", SqlDbType.Int).Value = transactionID;

                cmd.CommandText =
                    "INSERT INTO [dbo].[Order] (item_desc, item_qty, item_price, transaction_id) "
                    + "VALUES (@itemDesc, @itemQty, @itemPrice, @transactionID)";

                cmd.Connection = conn;

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }


            // Update Item quantity on stock
            cmd.Parameters.Add("item_name", SqlDbType.VarChar);
            cmd.Parameters.Add("item_qty", SqlDbType.Int);
            for (int i = 0; i < transaction.GetOrderItemQtyList().Count; i++)
            {
                cmd.Parameters["item_name"].Value = transaction.GetOrderItemNameList()[i];
                cmd.Parameters["item_qty"].Value = (int)transaction.GetOrderItemQtyList()[i];
                cmd.CommandText =
                    "UPDATE Item SET quantity = quantity - @item_qty where item_name = @item_name";

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }

            // 2. Decrease Stock quantity depending on orderqty
            MessageBox.Show("Success!");

            MainWindow mainWindow = (MainWindow)this.ParentForm;
            try
            {
                mainWindow.CheckoutInterrupt(false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error: Could not find MainWindow!");
                throw;
            }

            ClearForm();
        }

        private void ClearForm() {
            totalAmountDueTextBox.Text = "0.00";
            amountPayingNUD.Value = 0;
            cashDiscNUD.Value = 0;
            changeTextBox.Text = "0.00";
            customerNameTextBox.Text = "";
        }

        private void cashDiscNUD_ValueChanged(object sender, EventArgs e)
        {
            int val;
            if (!int.TryParse(cashDiscNUD.Value.ToString(), out val))
                cashDiscNUD.Value = 0;

            double cashDiscount = (double)cashDiscNUD.Value;

            transaction.SetCashDiscount(cashDiscount);

            totalAmountDueTextBox.Text = transaction.GetTotalAmountDue().ToString();
            changeTextBox.Text = transaction.GetChange().ToString();
        }

        private void customerNameTextBox_TextChanged(object sender, EventArgs e)
        {
            transaction.SetCustomerName(customerNameTextBox.Text);
            transaction.GetTotalAmountDue();
            transaction.GetAmountPaying();
            transaction.GetCashDiscount();
            transaction.GetChange();
        }

        private void cashDiscNUD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
                cashDiscNUD.Value = 0;
        }

        private void amountPayingNUD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
                amountPayingNUD.Value = 0;
        }
    }
}
