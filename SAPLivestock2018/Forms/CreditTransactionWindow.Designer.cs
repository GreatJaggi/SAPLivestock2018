﻿namespace SAPLivestock2018.Forms
{
    partial class CreditTransactionWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.WindowPane = new System.Windows.Forms.Panel();
            this.transactionDGV = new System.Windows.Forms.DataGridView();
            this.transactionidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customernameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalamountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountpayingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cashdiscountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.changeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transactionBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.sIIL2018DataSet = new SAPLivestock2018.SIIL2018DataSet();
            this.ItemSearchBox = new System.Windows.Forms.TextBox();
            this.transactionBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.transactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transactionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.transactionTableAdapter = new SAPLivestock2018.SIIL2018DataSetTableAdapters.TransactionTableAdapter();
            this.label3 = new System.Windows.Forms.Label();
            this.clearFilter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.WindowPane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search Customer Name";
            // 
            // WindowPane
            // 
            this.WindowPane.BackColor = System.Drawing.SystemColors.Control;
            this.WindowPane.Controls.Add(this.label3);
            this.WindowPane.Controls.Add(this.transactionDGV);
            this.WindowPane.Controls.Add(this.clearFilter);
            this.WindowPane.Controls.Add(this.ItemSearchBox);
            this.WindowPane.Controls.Add(this.label2);
            this.WindowPane.Controls.Add(this.label1);
            this.WindowPane.Controls.Add(this.endDate);
            this.WindowPane.Controls.Add(this.startDate);
            this.WindowPane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowPane.Location = new System.Drawing.Point(0, 0);
            this.WindowPane.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.WindowPane.Name = "WindowPane";
            this.WindowPane.Size = new System.Drawing.Size(1312, 690);
            this.WindowPane.TabIndex = 2;
            // 
            // transactionDGV
            // 
            this.transactionDGV.AllowUserToAddRows = false;
            this.transactionDGV.AllowUserToDeleteRows = false;
            this.transactionDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.transactionDGV.AutoGenerateColumns = false;
            this.transactionDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.transactionDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.transactionDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.transactionDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.transactionidDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.customernameDataGridViewTextBoxColumn,
            this.totalamountDataGridViewTextBoxColumn,
            this.amountpayingDataGridViewTextBoxColumn,
            this.cashdiscountDataGridViewTextBoxColumn,
            this.changeDataGridViewTextBoxColumn});
            this.transactionDGV.DataSource = this.transactionBindingSource3;
            this.transactionDGV.Location = new System.Drawing.Point(0, 39);
            this.transactionDGV.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.transactionDGV.Name = "transactionDGV";
            this.transactionDGV.ReadOnly = true;
            this.transactionDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.transactionDGV.Size = new System.Drawing.Size(1304, 647);
            this.transactionDGV.TabIndex = 2;
            this.transactionDGV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemDataGridView_CellDoubleClick);
            // 
            // transactionidDataGridViewTextBoxColumn
            // 
            this.transactionidDataGridViewTextBoxColumn.DataPropertyName = "transaction_id";
            this.transactionidDataGridViewTextBoxColumn.HeaderText = "Transaction ID";
            this.transactionidDataGridViewTextBoxColumn.Name = "transactionidDataGridViewTextBoxColumn";
            this.transactionidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "transaction_datetime";
            this.dataGridViewTextBoxColumn1.HeaderText = "Timestamp";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // customernameDataGridViewTextBoxColumn
            // 
            this.customernameDataGridViewTextBoxColumn.DataPropertyName = "customer_name";
            this.customernameDataGridViewTextBoxColumn.HeaderText = "Customer Name";
            this.customernameDataGridViewTextBoxColumn.Name = "customernameDataGridViewTextBoxColumn";
            this.customernameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalamountDataGridViewTextBoxColumn
            // 
            this.totalamountDataGridViewTextBoxColumn.DataPropertyName = "total_amount";
            this.totalamountDataGridViewTextBoxColumn.HeaderText = "Total Amount Due";
            this.totalamountDataGridViewTextBoxColumn.Name = "totalamountDataGridViewTextBoxColumn";
            this.totalamountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // amountpayingDataGridViewTextBoxColumn
            // 
            this.amountpayingDataGridViewTextBoxColumn.DataPropertyName = "amount_paying";
            this.amountpayingDataGridViewTextBoxColumn.HeaderText = "Amount Paid";
            this.amountpayingDataGridViewTextBoxColumn.Name = "amountpayingDataGridViewTextBoxColumn";
            this.amountpayingDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cashdiscountDataGridViewTextBoxColumn
            // 
            this.cashdiscountDataGridViewTextBoxColumn.DataPropertyName = "cash_discount";
            this.cashdiscountDataGridViewTextBoxColumn.HeaderText = "Discount";
            this.cashdiscountDataGridViewTextBoxColumn.Name = "cashdiscountDataGridViewTextBoxColumn";
            this.cashdiscountDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // changeDataGridViewTextBoxColumn
            // 
            this.changeDataGridViewTextBoxColumn.DataPropertyName = "change";
            this.changeDataGridViewTextBoxColumn.HeaderText = "Balance";
            this.changeDataGridViewTextBoxColumn.Name = "changeDataGridViewTextBoxColumn";
            this.changeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // transactionBindingSource3
            // 
            this.transactionBindingSource3.DataMember = "Transaction";
            this.transactionBindingSource3.DataSource = this.sIIL2018DataSet;
            // 
            // sIIL2018DataSet
            // 
            this.sIIL2018DataSet.DataSetName = "SIIL2018DataSet";
            this.sIIL2018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ItemSearchBox
            // 
            this.ItemSearchBox.Location = new System.Drawing.Point(235, 10);
            this.ItemSearchBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ItemSearchBox.Name = "ItemSearchBox";
            this.ItemSearchBox.Size = new System.Drawing.Size(191, 22);
            this.ItemSearchBox.TabIndex = 0;
            this.ItemSearchBox.TextChanged += new System.EventHandler(this.ItemSearchBox_TextChanged);
            // 
            // transactionBindingSource2
            // 
            this.transactionBindingSource2.DataMember = "Transaction";
            this.transactionBindingSource2.DataSource = this.sIIL2018DataSet;
            // 
            // transactionBindingSource
            // 
            this.transactionBindingSource.DataMember = "Transaction";
            this.transactionBindingSource.DataSource = this.sIIL2018DataSet;
            // 
            // transactionBindingSource1
            // 
            this.transactionBindingSource1.DataMember = "Transaction";
            this.transactionBindingSource1.DataSource = this.sIIL2018DataSet;
            // 
            // transactionTableAdapter
            // 
            this.transactionTableAdapter.ClearBeforeFill = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(468, 13);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "By Date";
            // 
            // clearFilter
            // 
            this.clearFilter.Location = new System.Drawing.Point(1145, 9);
            this.clearFilter.Name = "clearFilter";
            this.clearFilter.Size = new System.Drawing.Size(158, 23);
            this.clearFilter.TabIndex = 11;
            this.clearFilter.Text = "Clear Search Filters";
            this.clearFilter.UseVisualStyleBackColor = true;
            this.clearFilter.Click += new System.EventHandler(this.clearFilter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(713, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "TO";
            // 
            // endDate
            // 
            this.endDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDate.Location = new System.Drawing.Point(752, 12);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(150, 23);
            this.endDate.TabIndex = 9;
            this.endDate.ValueChanged += new System.EventHandler(this.endDate_ValueChanged);
            // 
            // startDate
            // 
            this.startDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDate.Location = new System.Drawing.Point(556, 12);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(150, 23);
            this.startDate.TabIndex = 8;
            this.startDate.ValueChanged += new System.EventHandler(this.startDate_ValueChanged);
            // 
            // CreditTransactionWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 690);
            this.Controls.Add(this.WindowPane);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CreditTransactionWindow";
            this.Text = "CreditTransactionWindow";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CreditTransactionWindow_Load);
            this.WindowPane.ResumeLayout(false);
            this.WindowPane.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel WindowPane;
        private System.Windows.Forms.TextBox ItemSearchBox;
        private SIIL2018DataSet sIIL2018DataSet;
        private System.Windows.Forms.BindingSource transactionBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customeridDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transactiondatetimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cashDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource transactionBindingSource1;
        private System.Windows.Forms.DataGridView transactionDGV;
        private System.Windows.Forms.BindingSource transactionBindingSource2;
        private System.Windows.Forms.BindingSource transactionBindingSource3;
        private SIIL2018DataSetTableAdapters.TransactionTableAdapter transactionTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn transactionidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn customernameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalamountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountpayingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cashdiscountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn changeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button clearFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.DateTimePicker startDate;
    }
}