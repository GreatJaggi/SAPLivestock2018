﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms
{
    public partial class CreditTransactionWindow : Form
    {
        public CreditTransactionWindow()
        {
            InitializeComponent();
        }

        private void CreditTransactionWindow_Load(object sender, EventArgs e)
        {
            TransactionDataGridViewBind();
        }

        public void TransactionDataGridViewBind()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Transaction] where change < 0", con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Item");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            transactionDGV.DataSource = bs;
            con.Close();
        }

        // Customer Name Search Box
        private void ItemSearchBox_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;
            bs.Filter = "change < 0 AND customer_name LIKE '%" + ItemSearchBox.Text + "%'";
            
            transactionDGV.DataSource = bs;
        }

        private void ItemDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // return if index header was selected
            if (e.RowIndex == -1) return;

            string transactionID = transactionDGV[0, e.RowIndex].Value.ToString();
            string customerName = transactionDGV[2, e.RowIndex].Value.ToString();
            string balance = transactionDGV[6, e.RowIndex].Value.ToString();
            string amountPaid = transactionDGV[4, e.RowIndex].Value.ToString();
            
            balance = (Convert.ToDouble(balance) * -1).ToString();
            
            External_Sub.CreditPayment creditPayment 
                = new External_Sub.CreditPayment(transactionID, customerName, balance, amountPaid);

            creditPayment.ShowDialog();
        }

        private void endDate_ValueChanged(object sender, EventArgs e)
        {
            DateFilter();
        }

        private void startDate_ValueChanged(object sender, EventArgs e)
        {
            DateFilter();
        }

        private void DateFilter()
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;

            bs.Filter = "transaction_datetime >= '" + startDate.Value.Date + "' AND " +
                        "transaction_datetime <= '" + endDate.Value.Date + "'";

            transactionDGV.DataSource = bs;
        }

        private void clearFilter_Click(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;
            bs.RemoveFilter();
            transactionDGV.DataSource = bs;
            TransactionDataGridViewBind();
        }
    }
}
