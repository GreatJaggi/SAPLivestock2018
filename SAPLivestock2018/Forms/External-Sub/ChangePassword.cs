﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SAPLivestock2018.Security;

namespace SAPLivestock2018.Forms.External_Sub
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void changePassBtn_Click(object sender, EventArgs e)
        {
            if (SAPSecurity.ChangePassword(passwordTextBox.Text, newPasswordTextBox.Text))
            {
                MessageBox.Show("Successfully changed your password.");
                if (SAPSecurity.IsAdmin())
                    MessageBox.Show("You will be logged out of your account.");
                SAPSecurity.LogOut();
                this.Close();
            }
            else MessageBox.Show("Error Changing Password [Wrong Password]");
            
        }
    }
}
