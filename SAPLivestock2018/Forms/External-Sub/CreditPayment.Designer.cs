﻿namespace SAPLivestock2018.Forms.External_Sub
{
    partial class CreditPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransactionIDLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.AcceptBtn = new System.Windows.Forms.Button();
            this.transactionIDTextBox = new System.Windows.Forms.TextBox();
            this.customerNameTextbox = new System.Windows.Forms.TextBox();
            this.balanceTextbox = new System.Windows.Forms.TextBox();
            this.amountPayingNUD = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.amountPayingNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // TransactionIDLabel
            // 
            this.TransactionIDLabel.AutoSize = true;
            this.TransactionIDLabel.Location = new System.Drawing.Point(13, 13);
            this.TransactionIDLabel.Name = "TransactionIDLabel";
            this.TransactionIDLabel.Size = new System.Drawing.Size(77, 13);
            this.TransactionIDLabel.TabIndex = 0;
            this.TransactionIDLabel.Text = "Transaction ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(105, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Balance Remaining";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Amount Paying";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Customer Name";
            // 
            // AcceptBtn
            // 
            this.AcceptBtn.Location = new System.Drawing.Point(235, 112);
            this.AcceptBtn.Name = "AcceptBtn";
            this.AcceptBtn.Size = new System.Drawing.Size(75, 23);
            this.AcceptBtn.TabIndex = 5;
            this.AcceptBtn.Text = "Accept";
            this.AcceptBtn.UseVisualStyleBackColor = true;
            this.AcceptBtn.Click += new System.EventHandler(this.AcceptBtn_Click);
            // 
            // transactionIDTextBox
            // 
            this.transactionIDTextBox.Location = new System.Drawing.Point(120, 10);
            this.transactionIDTextBox.Name = "transactionIDTextBox";
            this.transactionIDTextBox.ReadOnly = true;
            this.transactionIDTextBox.Size = new System.Drawing.Size(190, 20);
            this.transactionIDTextBox.TabIndex = 6;
            // 
            // customerNameTextbox
            // 
            this.customerNameTextbox.Location = new System.Drawing.Point(120, 32);
            this.customerNameTextbox.Name = "customerNameTextbox";
            this.customerNameTextbox.ReadOnly = true;
            this.customerNameTextbox.Size = new System.Drawing.Size(190, 20);
            this.customerNameTextbox.TabIndex = 7;
            // 
            // balanceTextbox
            // 
            this.balanceTextbox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.balanceTextbox.Location = new System.Drawing.Point(210, 60);
            this.balanceTextbox.Name = "balanceTextbox";
            this.balanceTextbox.ReadOnly = true;
            this.balanceTextbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.balanceTextbox.Size = new System.Drawing.Size(100, 20);
            this.balanceTextbox.TabIndex = 8;
            // 
            // amountPayingNUD
            // 
            this.amountPayingNUD.DecimalPlaces = 2;
            this.amountPayingNUD.Location = new System.Drawing.Point(210, 87);
            this.amountPayingNUD.Name = "amountPayingNUD";
            this.amountPayingNUD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.amountPayingNUD.Size = new System.Drawing.Size(100, 20);
            this.amountPayingNUD.TabIndex = 9;
            this.amountPayingNUD.ValueChanged += new System.EventHandler(this.amountPayingNUD_ValueChanged);
            // 
            // CreditPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(326, 144);
            this.Controls.Add(this.amountPayingNUD);
            this.Controls.Add(this.balanceTextbox);
            this.Controls.Add(this.customerNameTextbox);
            this.Controls.Add(this.transactionIDTextBox);
            this.Controls.Add(this.AcceptBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TransactionIDLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreditPayment";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Credit Transaction Payment";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.amountPayingNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TransactionIDLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button AcceptBtn;
        private System.Windows.Forms.TextBox transactionIDTextBox;
        private System.Windows.Forms.TextBox customerNameTextbox;
        private System.Windows.Forms.TextBox balanceTextbox;
        private System.Windows.Forms.NumericUpDown amountPayingNUD;
    }
}