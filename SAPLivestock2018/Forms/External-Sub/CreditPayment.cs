﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms.External_Sub
{
    public partial class CreditPayment : Form
    {

        double initialBalance;
        double amountPaying;
        double amountPaid;

        public CreditPayment(string transactionID, string customerName, string balance, string amountPaid)
        {
            InitializeComponent();
            InitializeValues(transactionID, customerName, balance, amountPaid);
        }

        void InitializeValues(string tID, string cName, string balance, string paid) {
            initialBalance = Convert.ToDouble(balance);
            amountPayingNUD.Maximum = (decimal)initialBalance;
            amountPayingNUD.Minimum = 0;

            transactionIDTextBox.Text = tID;
            customerNameTextbox.Text = cName;
            balanceTextbox.Text = balance;
            amountPaid = Convert.ToDouble(paid);
        }

        private void amountPayingNUD_ValueChanged(object sender, EventArgs e)
        {
            double balVal = initialBalance;
            balVal -= Convert.ToDouble(amountPayingNUD.Value);
            if (amountPayingNUD.Value < 0 || amountPayingNUD.Text == "") {
                amountPayingNUD.Value = 0;
                balanceTextbox.Text = initialBalance.ToString();
            }
            else balanceTextbox.Text = balVal.ToString();

            amountPaying = Convert.ToDouble(amountPayingNUD.Value);
        }

        private void AcceptBtn_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;

            // Update Values for credit transaction
            cmd.Parameters.Add("transactionID", SqlDbType.Int);
            cmd.Parameters.Add("balance", SqlDbType.Money);
            cmd.Parameters.Add("amount_paying", SqlDbType.Money);

            cmd.Parameters["transactionID"].Value = Convert.ToInt32(transactionIDTextBox.Text);
            cmd.Parameters["balance"].Value = (initialBalance - amountPaying) * -1;
            cmd.Parameters["amount_paying"].Value = amountPaid + amountPaying;

            cmd.CommandText =
                "UPDATE [dbo].[Transaction] SET change = @balance, " +
                "amount_paying = @amount_paying " +
                "where transaction_id = @transactionID";

            cmd.Connection = conn;
            conn.Open();
                cmd.ExecuteNonQuery();
            conn.Close();

            MessageBox.Show("Success!");
            this.Close();
        }
    }
}
