﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms.External_Sub
{
    public partial class LicenseWindow : Form
    {
        public LicenseWindow()
        {
            InitializeComponent();
            InitializeInformation();
        }

        private void InitializeInformation() {
            DisclaimerTextBox.Text =
            "Disclaimer \n" +
            "\nSales and Inventory system for Livestock Store is fully written and developed by John Zamora and is intended for private use of Ian Hendrick Tan." +
            "\n\nSource is private and shared between the two parties only and cannot be owned by other person people without any consent." +
            "\n\nIt is not applicable for public usage and or for personal gain without the consent of Ian Hendrick Tan.";
        }
    }
}
