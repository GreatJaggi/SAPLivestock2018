﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SAPLivestock2018.Security;

namespace SAPLivestock2018.Forms.External_Sub
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            SAPSecurity.LogIn(passwordTextBox.Text);

            if (SAPSecurity.IsAdmin())
            {

                this.Close();
            }
        }
    }
}
