﻿namespace SAPLivestock2018.Forms.External_Sub
{
    partial class StockCRUDWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.ItemIDNUD = new System.Windows.Forms.NumericUpDown();
            this.PriceNUD = new System.Windows.Forms.NumericUpDown();
            this.QuantityNUD = new System.Windows.Forms.NumericUpDown();
            this.CRUDButton = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CategoryComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DescTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ItemNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemIDNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuantityNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.DeleteBtn);
            this.panel1.Controls.Add(this.ItemIDNUD);
            this.panel1.Controls.Add(this.PriceNUD);
            this.panel1.Controls.Add(this.QuantityNUD);
            this.panel1.Controls.Add(this.CRUDButton);
            this.panel1.Controls.Add(this.ClearBtn);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.CategoryComboBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.DescTextBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ItemNameTextBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(355, 269);
            this.panel1.TabIndex = 0;
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(17, 231);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(75, 23);
            this.DeleteBtn.TabIndex = 19;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // ItemIDNUD
            // 
            this.ItemIDNUD.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ItemIDNUD.Location = new System.Drawing.Point(78, 17);
            this.ItemIDNUD.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.ItemIDNUD.Name = "ItemIDNUD";
            this.ItemIDNUD.ReadOnly = true;
            this.ItemIDNUD.Size = new System.Drawing.Size(265, 20);
            this.ItemIDNUD.TabIndex = 18;
            // 
            // PriceNUD
            // 
            this.PriceNUD.DecimalPlaces = 2;
            this.PriceNUD.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.PriceNUD.Location = new System.Drawing.Point(228, 197);
            this.PriceNUD.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.PriceNUD.Name = "PriceNUD";
            this.PriceNUD.Size = new System.Drawing.Size(115, 20);
            this.PriceNUD.TabIndex = 17;
            this.PriceNUD.ThousandsSeparator = true;
            // 
            // QuantityNUD
            // 
            this.QuantityNUD.Location = new System.Drawing.Point(78, 197);
            this.QuantityNUD.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.QuantityNUD.Name = "QuantityNUD";
            this.QuantityNUD.Size = new System.Drawing.Size(107, 20);
            this.QuantityNUD.TabIndex = 15;
            this.QuantityNUD.ThousandsSeparator = true;
            // 
            // CRUDButton
            // 
            this.CRUDButton.Location = new System.Drawing.Point(268, 231);
            this.CRUDButton.Name = "CRUDButton";
            this.CRUDButton.Size = new System.Drawing.Size(75, 23);
            this.CRUDButton.TabIndex = 14;
            this.CRUDButton.Text = "Add";
            this.CRUDButton.UseVisualStyleBackColor = true;
            this.CRUDButton.Click += new System.EventHandler(this.CRUDButton_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(187, 231);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(75, 23);
            this.ClearBtn.TabIndex = 13;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Price";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Quantity";
            // 
            // CategoryComboBox
            // 
            this.CategoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CategoryComboBox.FormattingEnabled = true;
            this.CategoryComboBox.Location = new System.Drawing.Point(78, 160);
            this.CategoryComboBox.Name = "CategoryComboBox";
            this.CategoryComboBox.Size = new System.Drawing.Size(265, 21);
            this.CategoryComboBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Category";
            // 
            // DescTextBox
            // 
            this.DescTextBox.Location = new System.Drawing.Point(78, 68);
            this.DescTextBox.Multiline = true;
            this.DescTextBox.Name = "DescTextBox";
            this.DescTextBox.Size = new System.Drawing.Size(265, 86);
            this.DescTextBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Description";
            // 
            // ItemNameTextBox
            // 
            this.ItemNameTextBox.Location = new System.Drawing.Point(78, 42);
            this.ItemNameTextBox.Name = "ItemNameTextBox";
            this.ItemNameTextBox.Size = new System.Drawing.Size(265, 20);
            this.ItemNameTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Item Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Item ID";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // StockCRUDWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 269);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StockCRUDWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Stock Item";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StockCRUDWindow_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemIDNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuantityNUD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox ItemNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button CRUDButton;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CategoryComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DescTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown QuantityNUD;
        private System.Windows.Forms.NumericUpDown PriceNUD;
        private System.Windows.Forms.NumericUpDown ItemIDNUD;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Timer timer1;
    }
}