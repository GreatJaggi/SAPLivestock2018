﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;
using System.Configuration;
using SAPLivestock2018.Security;

namespace SAPLivestock2018.Forms.External_Sub
{
    public enum Mode : int { Add=0, Edit, Delete };
    public partial class StockCRUDWindow : Form
    {
        private Mode mode;
        public StockCRUDWindow(Mode mode)
        {
            InitializeComponent();

            InitCatCB();
            switch (mode)
            {
                case (Mode.Add):
                    InitAddQuery(mode);
                    break;
                case (Mode.Edit):
                    InitEditQuery(mode);
                    break;
            }

            CategoryComboBox.SelectedIndex = 0;
        }

        private void InitAddQuery(Mode mode)
        {
            this.mode = mode;
            this.Text = "Add Stock Item";
            CRUDButton.Text = "Add";
            DeleteBtn.Hide();
        }

        private void InitEditQuery(Mode mode)
        {
            this.mode = mode;
            this.Text = "Edit Stock Item";
            CRUDButton.Text = "Edit";
            ClearBtn.Hide();
            DeleteBtn.Show();
        }
        
        private void InitCatCB()
        {
            CategoryComboBox.Items.Add("VT - Veterenary Products & Supplies");
            CategoryComboBox.Items.Add("CH - Chemicals");
            CategoryComboBox.Items.Add("FR - Feeds Retail / Wholesale");
            CategoryComboBox.Items.Add("GM - General Mose");
            
        }
        

        private void CRUDButton_Click(object sender, EventArgs e)
        {
            if (ItemNameTextBox.Text == "" || DescTextBox.Text == "" || PriceNUD.Value == 0 || PriceNUD.Text == "") {
                MessageBox.Show(this, "Invalid input parameters! Please check your input!");
                return;
            }

            switch (this.mode)
            {
                case (Mode.Add):
                    AddStockItem();
                    break;
                case (Mode.Edit):
                    EditStockItem();
                    break;
            }

            MessageBox.Show(this, "Input validated!\nplease refresh the window if changes doesn't appear yet.");
        }

        private void AddStockItem()
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            SqlCommand cmd = null;

            string cat = "";
            switch(CategoryComboBox.SelectedIndex)
            {
                case 0:
                    cat = "VT";
                    break;
                case 1:
                    cat = "CH";
                    break;
                case 2:
                    cat = "FR";
                    break;
                case 3:
                    cat = "GM";
                    break;
            }
            string cmdString =
                @"
    INSERT INTO [dbo].[Item]
           ([item_name]
           ,[description]
           ,[price]
           ,[quantity]
           ,[category])
     VALUES
           ('" + ItemNameTextBox.Text + "'," +
           "'" + DescTextBox.Text + "'," +
           "'" + PriceNUD.Value + "'," +
           "'" + QuantityNUD.Value + "'," +
           "'" + cat + "')";

            
            //Execution
            con.Open();
            cmd = new SqlCommand(cmdString, con);
            cmd.ExecuteNonQuery();
            con.Close();
            
                MessageBox.Show(this, "Success!");
                this.Close();

        }

        int item_id = 0;
        string item_name;
        string description;
        double price;
        int quantity;
        int category;

        private void EditStockItem()
        {
            this.Text = "Edit Item";

            string cat = "";
            switch (CategoryComboBox.SelectedIndex)
            {
                case 0:
                    cat = "VT";
                    break;
                case 1:
                    cat = "CH";
                    break;
                case 2:
                    cat = "FR";
                    break;
                case 3:
                    cat = "GM";
                    break;
            }


            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.Parameters.Add("item_id", SqlDbType.Int);
            cmd.Parameters.Add("item_name", SqlDbType.VarChar);
            cmd.Parameters.Add("description", SqlDbType.VarChar);
            cmd.Parameters.Add("price", SqlDbType.Money);
            cmd.Parameters.Add("quantity", SqlDbType.Int);
            cmd.Parameters.Add("category", SqlDbType.VarChar);

            cmd.Parameters["item_id"].Value = item_id;
            cmd.Parameters["item_name"].Value = ItemNameTextBox.Text;
            cmd.Parameters["description"].Value = DescTextBox.Text;
            cmd.Parameters["price"].Value = Convert.ToDouble(PriceNUD.Value);
            cmd.Parameters["quantity"].Value = QuantityNUD.Value;
            cmd.Parameters["category"].Value = cat;

            //Execution
            cmd.CommandText =
                "UPDATE [dbo].[Item] SET item_name = @item_name, description = @description, price = @price, quantity = @quantity, category = @category WHERE item_id = @item_id";

            Console.WriteLine(
             "UPDATE [dbo].[Item] SET item_name = " + item_name + ", " +
             "description = " + description + ", " +
             "price = " + price + ", " +
             "quantity = " + quantity + ", " +
             "category = @category WHERE item_id = " + item_id
            );
            cmd.Connection = con;
            con.Open();
                cmd.ExecuteNonQuery();
            con.Close();
            
            MessageBox.Show(this, "Successfully Edited Item!");
            this.Close();
        }

        public void SetAndShowParameters(int item_id, string item_name, string description, 
                                    double price, int quantity, int category) {
            this.item_id = item_id;
            this.item_name = item_name;
            this.description = description;
            this.price = price;
            this.quantity = quantity;
            this.category = category;

            ItemIDNUD.Value = (decimal)item_id;
            ItemNameTextBox.Text = item_name;
            DescTextBox.Text = description;
            PriceNUD.Value = (decimal)price;
            QuantityNUD.Value = (decimal)quantity;
            CategoryComboBox.SelectedIndex = category;
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            SqlCommand cmd = null;
            
            string cmdString =
                @"DELETE FROM [dbo].[Item] WHERE item_id = " + item_id;
               
            //Execution
            con.Open();
            cmd = new SqlCommand(cmdString, con);
            cmd.ExecuteNonQuery();
            con.Close();

            MessageBox.Show(this, "Success!");
            this.Close();
        }

        private void StockCRUDWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DeleteBtn.Enabled = SAPSecurity.IsAdmin();
            CRUDButton.Enabled = SAPSecurity.IsAdmin();
        }
    }
}
