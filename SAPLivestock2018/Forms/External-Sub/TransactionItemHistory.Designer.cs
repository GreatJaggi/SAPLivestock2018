﻿namespace SAPLivestock2018.Forms.External_Sub
{
    partial class TransactionItemHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemDGV = new System.Windows.Forms.DataGridView();
            this.voidBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.itemDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // itemDGV
            // 
            this.itemDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.itemDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.itemDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.itemDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemDGV.Location = new System.Drawing.Point(12, 28);
            this.itemDGV.Name = "itemDGV";
            this.itemDGV.Size = new System.Drawing.Size(319, 168);
            this.itemDGV.TabIndex = 0;
            // 
            // voidBtn
            // 
            this.voidBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.voidBtn.Location = new System.Drawing.Point(12, 202);
            this.voidBtn.Name = "voidBtn";
            this.voidBtn.Size = new System.Drawing.Size(75, 23);
            this.voidBtn.TabIndex = 1;
            this.voidBtn.Text = "VOID";
            this.voidBtn.UseVisualStyleBackColor = true;
            this.voidBtn.Click += new System.EventHandler(this.voidBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Transaction Items";
            // 
            // TransactionItemHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 230);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.voidBtn);
            this.Controls.Add(this.itemDGV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TransactionItemHistory";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TransactionItemHistory";
            ((System.ComponentModel.ISupportInitialize)(this.itemDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView itemDGV;
        private System.Windows.Forms.Button voidBtn;
        private System.Windows.Forms.Label label1;
    }
}