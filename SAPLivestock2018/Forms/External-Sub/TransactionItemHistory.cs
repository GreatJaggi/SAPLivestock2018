﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms.External_Sub
{
    public partial class TransactionItemHistory : Form
    {
        int transactionID;
        public TransactionItemHistory(int _transactionID)
        {
            InitializeComponent();
            transactionID = _transactionID;
            DGVInit();
        }
        
        public void DGVInit() {
            this.Text = "Transaction ID: " + transactionID.ToString();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Order]", con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Order");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            bs.Filter = "transaction_id = " + transactionID;

            itemDGV.DataSource = bs;

            itemDGV.Columns["order_id"].Visible = false;
            itemDGV.Columns["transaction_id"].Visible = false;
            itemDGV.Columns["item_desc"].HeaderText = "Item";
            itemDGV.Columns["item_qty"].HeaderText = "Quantity";
            itemDGV.Columns["item_price"].HeaderText = "Price";


            con.Close();
        }

        private void voidBtn_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure? You are trying to delete a transaction! TID: " + transactionID, "Authentication Required", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                //delete branch
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
                SqlCommand cmd = null;

                string cmdString =
                    @"DELETE FROM [dbo].[Order] WHERE transaction_id = " + transactionID;

                //Execution
                con.Open();
                cmd = new SqlCommand(cmdString, con);
                cmd.ExecuteNonQuery();
                con.Close();

                cmdString =
                    @"DELETE FROM [dbo].[Transaction] WHERE transaction_id = " + transactionID;

                //Execution
                con.Open();
                cmd = new SqlCommand(cmdString, con);
                cmd.ExecuteNonQuery();
                con.Close();

                // return the items that was voided in this part before closing
                /***/

                
                for(int i = 0; i < itemDGV.RowCount - 1; i++) {
                    con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
                    cmd = null;
                    
                    cmdString =
                        @"UPDATE [dbo].[Item] SET quantity = quantity + " + itemDGV.Rows[i].Cells["item_qty"].Value + " WHERE item_name = '" + itemDGV.Rows[i].Cells["item_desc"].Value + "'";

                    //Execution
                    con.Open();
                    cmd = new SqlCommand(cmdString, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

                this.Close();
            }

            else if (dialogResult == DialogResult.No)
            {
                //cancel delete
            }
        }
    }
}