﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms
{
    public partial class InventoryWindow : Form
    {
        public InventoryWindow()
        {
            InitializeComponent();
        }

        private void InventoryWindow_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sIIL2018DataSet.Item' table. You can move, or remove it, as needed.
            this.itemTableAdapter.Fill(this.sIIL2018DataSet.Item);

            ComputeTotalItems();
            ComputeStockValue();

        }

        public void ComputeTotalItems()
        {
            int totalItems = 0;
            for (int i = 0; i < ItemDataGridView.RowCount; i++) {
                totalItems += (int)ItemDataGridView.Rows[i].Cells[4].Value;
            }
            totalItemsTextBox.Text = String.Format("{0:n}", totalItems);
        }

        public void ComputeStockValue()
        {
            double stockValue = 0;
            for(int i = 0; i < ItemDataGridView.RowCount; i++) {
                stockValue += Convert.ToDouble(ItemDataGridView.Rows[i].Cells[4].Value) * Convert.ToDouble(ItemDataGridView.Rows[i].Cells[3].Value);
            }
            stockValueTextBox.Text = String.Format("{0:n}", stockValue);
        }

        public void ItemDataGridViewBind()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Item", con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Item");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            ItemDataGridView.DataSource = bs;
            con.Close();

            ComputeTotalItems();
            ComputeStockValue();
        }

        private void ItemSearchBox_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ItemDataGridView.DataSource;
            bs.Filter = "item_name LIKE '%" + ItemSearchBox.Text + "%'";
            ItemDataGridView.DataSource = bs;
        }
    }
}
