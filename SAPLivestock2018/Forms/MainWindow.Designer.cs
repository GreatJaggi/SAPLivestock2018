﻿namespace SAPLivestock2018.Forms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.TaskBar = new System.Windows.Forms.Panel();
            this.SalesMenuBtn = new System.Windows.Forms.Button();
            this.StocksMenuBtn = new System.Windows.Forms.Button();
            this.StoreMenuBtn = new System.Windows.Forms.Button();
            this.MainPane = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.storeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.storeMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewRecentTransactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewItemToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewItemToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshGridsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stocksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sIIL2018DataSet = new SAPLivestock2018.SIIL2018DataSet();
            this.stocksBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TaskBar.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stocksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stocksBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // TaskBar
            // 
            this.TaskBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TaskBar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TaskBar.Controls.Add(this.SalesMenuBtn);
            this.TaskBar.Controls.Add(this.StocksMenuBtn);
            this.TaskBar.Controls.Add(this.StoreMenuBtn);
            this.TaskBar.Location = new System.Drawing.Point(0, 22);
            this.TaskBar.Name = "TaskBar";
            this.TaskBar.Size = new System.Drawing.Size(984, 28);
            this.TaskBar.TabIndex = 0;
            this.TaskBar.Visible = false;
            // 
            // SalesMenuBtn
            // 
            this.SalesMenuBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SalesMenuBtn.Location = new System.Drawing.Point(909, 0);
            this.SalesMenuBtn.Name = "SalesMenuBtn";
            this.SalesMenuBtn.Size = new System.Drawing.Size(75, 50);
            this.SalesMenuBtn.TabIndex = 2;
            this.SalesMenuBtn.Text = "Sales";
            this.SalesMenuBtn.UseVisualStyleBackColor = true;
            // 
            // StocksMenuBtn
            // 
            this.StocksMenuBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StocksMenuBtn.Location = new System.Drawing.Point(828, 0);
            this.StocksMenuBtn.Name = "StocksMenuBtn";
            this.StocksMenuBtn.Size = new System.Drawing.Size(75, 50);
            this.StocksMenuBtn.TabIndex = 1;
            this.StocksMenuBtn.Text = "Stocks";
            this.StocksMenuBtn.UseVisualStyleBackColor = true;
            // 
            // StoreMenuBtn
            // 
            this.StoreMenuBtn.Location = new System.Drawing.Point(0, 0);
            this.StoreMenuBtn.Name = "StoreMenuBtn";
            this.StoreMenuBtn.Size = new System.Drawing.Size(75, 50);
            this.StoreMenuBtn.TabIndex = 0;
            this.StoreMenuBtn.Text = "Store Menu";
            this.StoreMenuBtn.UseVisualStyleBackColor = true;
            this.StoreMenuBtn.Click += new System.EventHandler(this.StoreMenuBtn_Click);
            // 
            // MainPane
            // 
            this.MainPane.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPane.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.MainPane.Location = new System.Drawing.Point(0, 22);
            this.MainPane.Name = "MainPane";
            this.MainPane.Size = new System.Drawing.Size(984, 539);
            this.MainPane.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.storeToolStripMenuItem,
            this.stocksToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.userToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.refreshGridsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // storeToolStripMenuItem
            // 
            this.storeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.storeMenuToolStripMenuItem,
            this.creditTransactionsToolStripMenuItem,
            this.viewRecentTransactionsToolStripMenuItem});
            this.storeToolStripMenuItem.Name = "storeToolStripMenuItem";
            this.storeToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.storeToolStripMenuItem.Text = "Store";
            // 
            // storeMenuToolStripMenuItem
            // 
            this.storeMenuToolStripMenuItem.Name = "storeMenuToolStripMenuItem";
            this.storeMenuToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.storeMenuToolStripMenuItem.Text = "Store Menu";
            this.storeMenuToolStripMenuItem.Click += new System.EventHandler(this.storeMenuToolStripMenuItem_Click);
            // 
            // creditTransactionsToolStripMenuItem
            // 
            this.creditTransactionsToolStripMenuItem.Name = "creditTransactionsToolStripMenuItem";
            this.creditTransactionsToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.creditTransactionsToolStripMenuItem.Text = "Credit Transactions";
            this.creditTransactionsToolStripMenuItem.Click += new System.EventHandler(this.creditTransactionsToolStripMenuItem_Click);
            // 
            // viewRecentTransactionsToolStripMenuItem
            // 
            this.viewRecentTransactionsToolStripMenuItem.Name = "viewRecentTransactionsToolStripMenuItem";
            this.viewRecentTransactionsToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.viewRecentTransactionsToolStripMenuItem.Text = "View Recent Transactions";
            this.viewRecentTransactionsToolStripMenuItem.Click += new System.EventHandler(this.viewRecentTransactionsToolStripMenuItem_Click);
            // 
            // stocksToolStripMenuItem
            // 
            this.stocksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewItemToolStrip,
            this.addNewItemToolStripMenuItem1});
            this.stocksToolStripMenuItem.Name = "stocksToolStripMenuItem";
            this.stocksToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.stocksToolStripMenuItem.Text = "Stocks";
            // 
            // viewItemToolStrip
            // 
            this.viewItemToolStrip.Name = "viewItemToolStrip";
            this.viewItemToolStrip.Size = new System.Drawing.Size(152, 22);
            this.viewItemToolStrip.Text = "View Inventory";
            this.viewItemToolStrip.Click += new System.EventHandler(this.viewItemToolStrip_Click);
            // 
            // addNewItemToolStripMenuItem1
            // 
            this.addNewItemToolStripMenuItem1.Name = "addNewItemToolStripMenuItem1";
            this.addNewItemToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.addNewItemToolStripMenuItem1.Text = "Add new item";
            this.addNewItemToolStripMenuItem1.Click += new System.EventHandler(this.addNewItemToolStripMenuItem1_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesToolStripMenuItem,
            this.inventoryToolStripMenuItem});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.salesToolStripMenuItem.Text = "Sales";
            this.salesToolStripMenuItem.Click += new System.EventHandler(this.salesToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            this.inventoryToolStripMenuItem.Click += new System.EventHandler(this.inventoryToolStripMenuItem_Click);
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.securityToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.changePasswordToolStripMenuItem});
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.userToolStripMenuItem.Text = "User";
            // 
            // securityToolStripMenuItem
            // 
            this.securityToolStripMenuItem.Name = "securityToolStripMenuItem";
            this.securityToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.securityToolStripMenuItem.Text = "Log-In";
            this.securityToolStripMenuItem.Click += new System.EventHandler(this.securityToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.licenseToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // licenseToolStripMenuItem
            // 
            this.licenseToolStripMenuItem.Name = "licenseToolStripMenuItem";
            this.licenseToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.licenseToolStripMenuItem.Text = "License";
            this.licenseToolStripMenuItem.Click += new System.EventHandler(this.licenseToolStripMenuItem_Click);
            // 
            // refreshGridsToolStripMenuItem
            // 
            this.refreshGridsToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.refreshGridsToolStripMenuItem.Name = "refreshGridsToolStripMenuItem";
            this.refreshGridsToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.refreshGridsToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.refreshGridsToolStripMenuItem.Text = "Refresh Grids";
            this.refreshGridsToolStripMenuItem.Click += new System.EventHandler(this.refreshGridsToolStripMenuItem_Click);
            // 
            // stocksBindingSource
            // 
            this.stocksBindingSource.DataMember = "Stocks";
            this.stocksBindingSource.DataSource = this.sIIL2018DataSet;
            // 
            // sIIL2018DataSet
            // 
            this.sIIL2018DataSet.DataSetName = "SIIL2018DataSet";
            this.sIIL2018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // stocksBindingSource1
            // 
            this.stocksBindingSource1.DataMember = "Stocks";
            this.stocksBindingSource1.DataSource = this.sIIL2018DataSet;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.logOutToolStripMenuItem.Text = "Log-Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.TaskBar);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.MainPane);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Sales and Inventory";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.TaskBar.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stocksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stocksBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel TaskBar;
        private System.Windows.Forms.Panel MainPane;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button SalesMenuBtn;
        private System.Windows.Forms.Button StocksMenuBtn;
        private System.Windows.Forms.Button StoreMenuBtn;
        private SIIL2018DataSet sIIL2018DataSet;
        private System.Windows.Forms.BindingSource stocksBindingSource;
        private System.Windows.Forms.BindingSource stocksBindingSource1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem storeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewItemToolStrip;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewItemToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem storeMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewRecentTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditTransactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshGridsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}