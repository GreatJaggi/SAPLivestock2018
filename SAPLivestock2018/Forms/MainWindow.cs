﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SAPLivestock2018.Security;

namespace SAPLivestock2018.Forms
{
    public partial class MainWindow : Form
    {
        private StockWindow stockWindow = new StockWindow();
        private StoreWindow storeWindow = new StoreWindow();
        private SalesWindow salesWindow = new SalesWindow();
        private InventoryWindow inventoryWindow = new InventoryWindow();
        private CheckoutWindow checkoutWindow = new CheckoutWindow();
        private CreditTransactionWindow creditTransactionWindow = new CreditTransactionWindow();
        private RecentTransactionWindow recentTransactionWindow = new RecentTransactionWindow();
        

        private static bool isCheckoutOpen;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            InitializePCWindows();
            UpdateUserRestrictions();
        }

        private void UpdateUserRestrictions()
        {
            reportToolStripMenuItem.Enabled = Security.SAPSecurity.IsAdmin();
            addNewItemToolStripMenuItem1.Enabled = Security.SAPSecurity.IsAdmin();

            securityToolStripMenuItem.Enabled = !Security.SAPSecurity.IsAdmin();
            logOutToolStripMenuItem.Enabled = Security.SAPSecurity.IsAdmin();
        }


        private void InitializePCWindows()
        {
            //Initialize panel controlled windows

            stockWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            storeWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            salesWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            inventoryWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            checkoutWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            creditTransactionWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            recentTransactionWindow.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            stockWindow.TopLevel = false;
            storeWindow.TopLevel = false;
            salesWindow.TopLevel = false;
            inventoryWindow.TopLevel = false;
            checkoutWindow.TopLevel = false;
            creditTransactionWindow.TopLevel = false;
            recentTransactionWindow.TopLevel = false;

            stockWindow.AutoScroll = true;
            storeWindow.AutoScroll = true;
            salesWindow.AutoScroll = true;
            inventoryWindow.AutoScroll = true;
            checkoutWindow.AutoScroll = true;
            creditTransactionWindow.AutoScroll = true;
            recentTransactionWindow.AutoScroll = true;

            MainPane.Controls.Add(stockWindow);
            MainPane.Controls.Add(storeWindow);
            MainPane.Controls.Add(salesWindow);
            MainPane.Controls.Add(inventoryWindow);
            MainPane.Controls.Add(checkoutWindow);
            MainPane.Controls.Add(creditTransactionWindow);
            MainPane.Controls.Add(recentTransactionWindow);

            // Show default window on init
            stockWindow.Hide();
            storeWindow.Show();
            salesWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            checkoutWindow.Hide();
            recentTransactionWindow.Hide();

            isCheckoutOpen = false;
        }


        private void StoreMenuBtn_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }

            storeWindow.Show();
            salesWindow.Hide();
            stockWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Hide();
        }

        private void viewItemToolStrip_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }
            storeWindow.Hide();
            salesWindow.Hide();
            stockWindow.Show();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Hide();
        }

        private void addNewItemToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            External_Sub.StockCRUDWindow sCRUD = new External_Sub.StockCRUDWindow(External_Sub.Mode.Add);
            sCRUD.ShowDialog();
        }

        private void storeMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }
            storeWindow.Show();
            salesWindow.Hide();
            stockWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Hide();
        }

        public void InitiateCheckout(Transaction transaction)
        {
            checkoutWindow.SetTransaction(transaction);
            checkoutWindow.InitiateCheckoutLocal();
            storeWindow.Hide();
            salesWindow.Hide();
            stockWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Hide();
            checkoutWindow.Show();
            isCheckoutOpen = true;
        }

        private void CheckoutInterrupt()
        {
            MessageBox.Show("Unable to switch window on Checkout! Please finish the transaction first.");
        }

        public void CheckoutInterrupt(bool cancelCheckout)
        {
            if (cancelCheckout)
            {
                DialogResult dialogResult = MessageBox.Show("You're about to cancel the current transaction(s). Are you sure?", "Cancel Checkout", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    isCheckoutOpen = false;
                    storeWindow.Show();
                    salesWindow.Hide();
                    stockWindow.Hide();
                    inventoryWindow.Hide();
                    creditTransactionWindow.Hide();
                    checkoutWindow.Hide();
                    recentTransactionWindow.Hide();
                    storeWindow.ClearOrderList();
                }
            }
            else
            {
                isCheckoutOpen = false;
                storeWindow.Show();
                salesWindow.Hide();
                stockWindow.Hide();
                inventoryWindow.Hide();
                creditTransactionWindow.Hide();
                recentTransactionWindow.Hide();
                checkoutWindow.Hide();
                storeWindow.ClearOrderList();
            }
        }

        private void creditTransactionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }

            storeWindow.Hide();
            salesWindow.Hide();
            stockWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Show();
            recentTransactionWindow.Hide();
        }

        private void viewRecentTransactionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }

            storeWindow.Hide();
            salesWindow.Hide();
            stockWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Show();
        }

        private void licenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            External_Sub.LicenseWindow licenseWindow = new External_Sub.LicenseWindow();
            licenseWindow.ShowDialog();
        }
        
        public void RefreshGridViews() {
            stockWindow.ItemDataGridViewBind();
            storeWindow.ItemDataGridViewBind();
            inventoryWindow.ItemDataGridViewBind();
            // Add sales window refresh here
            recentTransactionWindow.TransactionDataGridViewBind();
            creditTransactionWindow.TransactionDataGridViewBind();
        }

        private void salesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }
            storeWindow.Hide();
            salesWindow.Show();
            stockWindow.Hide();
            inventoryWindow.Hide();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Hide();
        }

        private void inventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isCheckoutOpen)
            {
                CheckoutInterrupt();
                return;
            }
            storeWindow.Hide();
            salesWindow.Hide();
            stockWindow.Hide();
            inventoryWindow.Show();
            creditTransactionWindow.Hide();
            recentTransactionWindow.Hide();
        }

        // START SINGLETON INSTANTIATION
        private static readonly MainWindow _mainWindow = new MainWindow();

        public static MainWindow GetMainWindow()
        {
            return _mainWindow;
        }
        // END SINGLETON CALLOUT

        private void refreshGridsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshGridViews();
        }

        private void securityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            External_Sub.Login logForm = new External_Sub.Login();
            logForm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateUserRestrictions();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SAPSecurity.LogOut();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            External_Sub.ChangePassword changePassForm = new External_Sub.ChangePassword();
            changePassForm.Show();
        }
    }
}
