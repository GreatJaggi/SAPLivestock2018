﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms
{
    public partial class RecentTransactionWindow : Form
    {
        public RecentTransactionWindow()
        {
            InitializeComponent();
        }

        private void RecentTransactionWindow_Load(object sender, EventArgs e)
        {
            TransactionDataGridViewBind();
        }

        public void TransactionDataGridViewBind()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM [Transaction]", con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Item");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            transactionDGV.DataSource = bs;
            con.Close();
        }

        private void transactionSearchBox_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;
            //bs.Filter = "\'" + transactionDGV.Columns[1].HeaderText.ToString() + "\'" + " LIKE '%" + transactionSearchBox.Text + "%'";
            //bs.Filter = "SELECT * WHERE 'Customer Name' = '" + transactionSearchBox.Text + "'";
            bs.Filter = "customer_name like '%" + transactionSearchBox.Text + "%'";

            transactionDGV.DataSource = bs;
        }

        private void transactionDGV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // return if index header was selected
            if (e.RowIndex == -1) return;

            int transaction_id = (int)transactionDGV.Rows[e.RowIndex].Cells[0].Value;

            External_Sub.TransactionItemHistory transactionItemHistory
                = new External_Sub.TransactionItemHistory(transaction_id);

            transactionItemHistory.ShowDialog();

            /*
            
            */
            
            
            
        }

        private void endDate_ValueChanged(object sender, EventArgs e)
        {
            DateFilter();
        }

        private void startDate_ValueChanged(object sender, EventArgs e)
        {
            DateFilter();
        }

        private void DateFilter() {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;

            bs.Filter = "transaction_datetime >= '" + startDate.Value.Date + "' AND " +
                        "transaction_datetime <= '" + endDate.Value.Date + "'";

            transactionDGV.DataSource = bs;
        }

        private void clearFilter_Click(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;
            bs.RemoveFilter();
            transactionDGV.DataSource = bs;
        }
    }
}
