﻿namespace SAPLivestock2018.Forms
{
    partial class SalesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sIIL2018DataSet = new SAPLivestock2018.SIIL2018DataSet();
            this.itemTableAdapter = new SAPLivestock2018.SIIL2018DataSetTableAdapters.ItemTableAdapter();
            this.WindowPane = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.printBtn = new System.Windows.Forms.Button();
            this.rawIncome2TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.totalIncomeTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rawIncome1TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.creditTransactionTextBox = new System.Windows.Forms.TextBox();
            this.stockValueSoldTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.clearFilter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.transactionDGV = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.transactionTableAdapter = new SAPLivestock2018.SIIL2018DataSetTableAdapters.TransactionTableAdapter();
            this.transactionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.siiL2018DataSet1 = new SAPLivestock2018.SIIL2018DataSet();
            this.transactionBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.transactionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cashBeginNUD = new System.Windows.Forms.NumericUpDown();
            this.miscFeeNUD = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).BeginInit();
            this.WindowPane.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siiL2018DataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashBeginNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.miscFeeNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // itemBindingSource
            // 
            this.itemBindingSource.DataMember = "Item";
            this.itemBindingSource.DataSource = this.sIIL2018DataSet;
            // 
            // sIIL2018DataSet
            // 
            this.sIIL2018DataSet.DataSetName = "SIIL2018DataSet";
            this.sIIL2018DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // itemTableAdapter
            // 
            this.itemTableAdapter.ClearBeforeFill = true;
            // 
            // WindowPane
            // 
            this.WindowPane.BackColor = System.Drawing.SystemColors.Control;
            this.WindowPane.Controls.Add(this.panel1);
            this.WindowPane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WindowPane.Location = new System.Drawing.Point(0, 0);
            this.WindowPane.Name = "WindowPane";
            this.WindowPane.Size = new System.Drawing.Size(984, 561);
            this.WindowPane.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.miscFeeNUD);
            this.panel1.Controls.Add(this.cashBeginNUD);
            this.panel1.Controls.Add(this.printBtn);
            this.panel1.Controls.Add(this.rawIncome2TextBox);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.totalIncomeTextBox);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.rawIncome1TextBox);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.creditTransactionTextBox);
            this.panel1.Controls.Add(this.stockValueSoldTextBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.clearFilter);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.endDate);
            this.panel1.Controls.Add(this.startDate);
            this.panel1.Controls.Add(this.transactionDGV);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 561);
            this.panel1.TabIndex = 4;
            // 
            // printBtn
            // 
            this.printBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.printBtn.Location = new System.Drawing.Point(875, 529);
            this.printBtn.Name = "printBtn";
            this.printBtn.Size = new System.Drawing.Size(75, 23);
            this.printBtn.TabIndex = 29;
            this.printBtn.Text = "Print Details";
            this.printBtn.UseVisualStyleBackColor = true;
            // 
            // rawIncome2TextBox
            // 
            this.rawIncome2TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rawIncome2TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rawIncome2TextBox.Location = new System.Drawing.Point(647, 480);
            this.rawIncome2TextBox.Name = "rawIncome2TextBox";
            this.rawIncome2TextBox.ReadOnly = true;
            this.rawIncome2TextBox.Size = new System.Drawing.Size(189, 21);
            this.rawIncome2TextBox.TabIndex = 27;
            this.rawIncome2TextBox.Text = "000";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(547, 483);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 17);
            this.label9.TabIndex = 26;
            this.label9.Text = "Income (Raw)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(509, 435);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "Miscellaneous Fees";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(561, 408);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 17);
            this.label12.TabIndex = 20;
            this.label12.Text = "Cash Begin";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // totalIncomeTextBox
            // 
            this.totalIncomeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.totalIncomeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalIncomeTextBox.Location = new System.Drawing.Point(647, 529);
            this.totalIncomeTextBox.Name = "totalIncomeTextBox";
            this.totalIncomeTextBox.ReadOnly = true;
            this.totalIncomeTextBox.Size = new System.Drawing.Size(189, 21);
            this.totalIncomeTextBox.TabIndex = 19;
            this.totalIncomeTextBox.Text = "000";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(552, 532);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 17);
            this.label7.TabIndex = 18;
            this.label7.Text = "Total Income";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rawIncome1TextBox
            // 
            this.rawIncome1TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rawIncome1TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rawIncome1TextBox.Location = new System.Drawing.Point(264, 480);
            this.rawIncome1TextBox.Name = "rawIncome1TextBox";
            this.rawIncome1TextBox.ReadOnly = true;
            this.rawIncome1TextBox.Size = new System.Drawing.Size(189, 21);
            this.rawIncome1TextBox.TabIndex = 17;
            this.rawIncome1TextBox.Text = "000";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(164, 483);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "Income (Raw)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // creditTransactionTextBox
            // 
            this.creditTransactionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.creditTransactionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditTransactionTextBox.Location = new System.Drawing.Point(264, 432);
            this.creditTransactionTextBox.Name = "creditTransactionTextBox";
            this.creditTransactionTextBox.ReadOnly = true;
            this.creditTransactionTextBox.Size = new System.Drawing.Size(189, 21);
            this.creditTransactionTextBox.TabIndex = 11;
            this.creditTransactionTextBox.Text = "000";
            // 
            // stockValueSoldTextBox
            // 
            this.stockValueSoldTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.stockValueSoldTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockValueSoldTextBox.Location = new System.Drawing.Point(264, 405);
            this.stockValueSoldTextBox.Name = "stockValueSoldTextBox";
            this.stockValueSoldTextBox.ReadOnly = true;
            this.stockValueSoldTextBox.Size = new System.Drawing.Size(189, 21);
            this.stockValueSoldTextBox.TabIndex = 10;
            this.stockValueSoldTextBox.Text = "000";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(147, 434);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Credit Balances";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(143, 408);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Stock Value Sold";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "By Date";
            // 
            // clearFilter
            // 
            this.clearFilter.Location = new System.Drawing.Point(856, 6);
            this.clearFilter.Margin = new System.Windows.Forms.Padding(2);
            this.clearFilter.Name = "clearFilter";
            this.clearFilter.Size = new System.Drawing.Size(118, 19);
            this.clearFilter.TabIndex = 6;
            this.clearFilter.Text = "Clear Search Filters";
            this.clearFilter.UseVisualStyleBackColor = true;
            this.clearFilter.Click += new System.EventHandler(this.clearFilter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(226, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "TO";
            // 
            // endDate
            // 
            this.endDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDate.Location = new System.Drawing.Point(256, 7);
            this.endDate.Margin = new System.Windows.Forms.Padding(2);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(114, 20);
            this.endDate.TabIndex = 4;
            this.endDate.Value = new System.DateTime(2019, 4, 28, 0, 0, 0, 0);
            this.endDate.ValueChanged += new System.EventHandler(this.endDate_ValueChanged);
            // 
            // startDate
            // 
            this.startDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDate.Location = new System.Drawing.Point(109, 7);
            this.startDate.Margin = new System.Windows.Forms.Padding(2);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(114, 20);
            this.startDate.TabIndex = 3;
            this.startDate.ValueChanged += new System.EventHandler(this.startDate_ValueChanged);
            // 
            // transactionDGV
            // 
            this.transactionDGV.AllowUserToAddRows = false;
            this.transactionDGV.AllowUserToDeleteRows = false;
            this.transactionDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.transactionDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.transactionDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.transactionDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.transactionDGV.Location = new System.Drawing.Point(3, 33);
            this.transactionDGV.Name = "transactionDGV";
            this.transactionDGV.ReadOnly = true;
            this.transactionDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.transactionDGV.Size = new System.Drawing.Size(978, 359);
            this.transactionDGV.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filter";
            // 
            // transactionTableAdapter
            // 
            this.transactionTableAdapter.ClearBeforeFill = true;
            // 
            // transactionBindingSource1
            // 
            this.transactionBindingSource1.DataMember = "Transaction";
            this.transactionBindingSource1.DataSource = this.siiL2018DataSet1;
            // 
            // siiL2018DataSet1
            // 
            this.siiL2018DataSet1.DataSetName = "SIIL2018DataSet";
            this.siiL2018DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // transactionBindingSource2
            // 
            this.transactionBindingSource2.DataMember = "Transaction";
            this.transactionBindingSource2.DataSource = this.siiL2018DataSet1;
            // 
            // transactionBindingSource
            // 
            this.transactionBindingSource.DataMember = "Transaction";
            this.transactionBindingSource.DataSource = this.siiL2018DataSet1;
            // 
            // cashBeginNUD
            // 
            this.cashBeginNUD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cashBeginNUD.DecimalPlaces = 2;
            this.cashBeginNUD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.cashBeginNUD.Location = new System.Drawing.Point(647, 408);
            this.cashBeginNUD.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.cashBeginNUD.Name = "cashBeginNUD";
            this.cashBeginNUD.Size = new System.Drawing.Size(189, 20);
            this.cashBeginNUD.TabIndex = 30;
            this.cashBeginNUD.ValueChanged += new System.EventHandler(this.cashBeginNUD_ValueChanged);
            // 
            // miscFeeNUD
            // 
            this.miscFeeNUD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.miscFeeNUD.DecimalPlaces = 2;
            this.miscFeeNUD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.miscFeeNUD.Location = new System.Drawing.Point(647, 434);
            this.miscFeeNUD.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.miscFeeNUD.Name = "miscFeeNUD";
            this.miscFeeNUD.Size = new System.Drawing.Size(189, 20);
            this.miscFeeNUD.TabIndex = 31;
            this.miscFeeNUD.ValueChanged += new System.EventHandler(this.cashBeginNUD_ValueChanged);
            // 
            // SalesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.WindowPane);
            this.Name = "SalesWindow";
            this.Text = "SalesWindow";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SalesWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sIIL2018DataSet)).EndInit();
            this.WindowPane.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siiL2018DataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashBeginNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.miscFeeNUD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private SIIL2018DataSet sIIL2018DataSet;
        private System.Windows.Forms.BindingSource itemBindingSource;
        private SIIL2018DataSetTableAdapters.ItemTableAdapter itemTableAdapter;
        private System.Windows.Forms.Panel WindowPane;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button clearFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.DateTimePicker startDate;
        private System.Windows.Forms.DataGridView transactionDGV;
        private System.Windows.Forms.BindingSource transactionBindingSource2;
        private SIIL2018DataSet siiL2018DataSet1;
        private SIIL2018DataSetTableAdapters.TransactionTableAdapter transactionTableAdapter;
        private System.Windows.Forms.BindingSource transactionBindingSource1;
        private System.Windows.Forms.BindingSource transactionBindingSource;
        private System.Windows.Forms.TextBox creditTransactionTextBox;
        private System.Windows.Forms.TextBox stockValueSoldTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox rawIncome1TextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox totalIncomeTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox rawIncome2TextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button printBtn;
        private System.Windows.Forms.NumericUpDown miscFeeNUD;
        private System.Windows.Forms.NumericUpDown cashBeginNUD;
    }
}