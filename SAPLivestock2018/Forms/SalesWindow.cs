﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms
{
    public partial class SalesWindow : Form
    {
        public SalesWindow()
        {
            InitializeComponent();
        }

        private void SalesWindow_Load(object sender, EventArgs e)
        {
            TransactionDataGridViewBind();

        }

        private float stockValueSold;
        private float creditTransaction;
        private float rawIncome;
        private float cashBegin;
        private float miscFee;
        private float totalIncome;

        public void TransactionDataGridViewBind()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter(
                "SELECT [Order].order_id, [Order].item_desc, [Order].item_qty, [Order].item_price, [Transaction].transaction_datetime FROM [Order] INNER JOIN [Transaction] ON [Order].transaction_id = [Transaction].transaction_id;"
                , con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Transaction");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            transactionDGV.DataSource = bs;
            transactionDGV.Columns[0].HeaderText = "Order ID";
            transactionDGV.Columns[1].HeaderText = "Description";
            transactionDGV.Columns[2].HeaderText = "Quantity";
            transactionDGV.Columns[3].HeaderText = "Price";
            transactionDGV.Columns[4].HeaderText = "Timestamp";

            con.Close();

            DateFilter();
            ComputeValues();
        }

        public void ComputeValues()
        {
            rawIncome = 0;
            stockValueSold = 0;

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();
            SqlCommand query = new SqlCommand("SELECT SUM(change) FROM [Transaction] WHERE change < 0");
            query.Connection = con;
            Console.WriteLine(query.ExecuteScalar());
            float sum = 0;
            try
            {
                sum = float.Parse(query.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
                MessageBox.Show("There are no transactions yet!");
                sum = 0;
            }
            

            for (int i = 0; i < transactionDGV.RowCount; i++)
            {
                float price;
                float quantity;
                
                try
                {
                    float.TryParse(transactionDGV.Rows[i].Cells[3].Value.ToString(), out price);
                    float.TryParse(transactionDGV.Rows[i].Cells[2].Value.ToString(), out quantity);
                }
                catch (Exception)
                {
                    Console.WriteLine("Error: No values");
                    price = 0;
                    quantity = 0;
                }

                stockValueSold += price * quantity;

                rawIncome += stockValueSold;
            }
            
            stockValueSoldTextBox.Text = ToCurrencyString(stockValueSold);
            creditTransactionTextBox.Text = ToCurrencyString(sum * -1);

            rawIncome = rawIncome - (sum * -1);
            totalIncome = rawIncome - (cashBegin + miscFee);

            totalIncomeTextBox.Text = ToCurrencyString(totalIncome);
            rawIncome1TextBox.Text = ToCurrencyString(rawIncome);
            rawIncome2TextBox.Text = ToCurrencyString(rawIncome);
        }

        private void clearFilter_Click(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;
            bs.RemoveFilter();
            transactionDGV.DataSource = bs;
        }

        private void startDate_ValueChanged(object sender, EventArgs e)
        {
            DateFilter();
            ComputeValues();
        }

        private void endDate_ValueChanged(object sender, EventArgs e)
        {
            DateFilter();
            ComputeValues();
        }

        private void DateFilter()
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = transactionDGV.DataSource;

            bs.Filter = "transaction_datetime >= '" + startDate.Value.Date + "' AND " +
                        "transaction_datetime <= '" + endDate.Value.Date + "'";

            transactionDGV.DataSource = bs;
        }

        public string ToCurrencyString(float text)
        {
            return String.Format("{0:n}", text);
        }

        private void cashBeginNUD_ValueChanged(object sender, EventArgs e)
        {
            cashBegin = (float)cashBeginNUD.Value;
            miscFee = (float)miscFeeNUD.Value;
            ComputeValues();
        }
    }
}
