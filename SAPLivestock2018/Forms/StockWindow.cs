﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAPLivestock2018.Forms
{
    public partial class StockWindow : Form
    {
        public StockWindow()
        {
            InitializeComponent();
            ItemDataGridViewBind();
        }
        
        private void StockWindow_Load(object sender, EventArgs e)
        {
            ItemDataGridViewBind();
        }

        private void ItemSearchBox_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ItemDataGridView.DataSource;
            bs.Filter = "item_name LIKE '%" + ItemSearchBox.Text + "%'";
            ItemDataGridView.DataSource = bs;
        }

        private void AddItemBtn_Click(object sender, EventArgs e)
        {
            External_Sub.StockCRUDWindow sCRUD = new External_Sub.StockCRUDWindow(External_Sub.Mode.Add);
            sCRUD.ShowDialog();
        }

        private void ItemDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // return if index header was selected
            if (e.RowIndex == -1) return;

            // Get gridview row value
            int item_id = Convert.ToInt32(ItemDataGridView[0, e.RowIndex].Value.ToString());
            string item_name = ItemDataGridView[1, e.RowIndex].Value.ToString();
            string description = ItemDataGridView[2, e.RowIndex].Value.ToString();
            double price = Convert.ToDouble(ItemDataGridView[3, e.RowIndex].Value.ToString());
            int quantity = Convert.ToInt32(ItemDataGridView[4, e.RowIndex].Value.ToString());
            string categoryString = ItemDataGridView[5, e.RowIndex].Value.ToString();

            int category = 0;

            if (categoryString == "VT")
                category = 0;
            else if (categoryString == "CH")
                category = 1;
            else if (categoryString == "FR")
                category = 2;
            else if (categoryString == "GM")
                category = 3;
                

            External_Sub.StockCRUDWindow sCRUD = new External_Sub.StockCRUDWindow(External_Sub.Mode.Edit);

            sCRUD.SetAndShowParameters(item_id, item_name, description, price, quantity, category);

            sCRUD.ShowDialog();
        }

        public void RefreshView() {
            ItemDataGridView.Refresh();
        }

        public void ItemDataGridViewBind() {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Item", con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Item");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            ItemDataGridView.DataSource = bs;
            con.Close();
            PopulateQuantityColors();
        }

        public void PopulateQuantityColors() {
            // Populate Quantity Filters by Cell Color
            int rowCount = ItemDataGridView.RowCount;
            for (int i = 0; i < rowCount; i++)
            {
                int value = (int)ItemDataGridView.Rows[i].Cells[4].Value;
                if (value > 150)
                    ItemDataGridView.Rows[i].Cells[4].Style.BackColor = Color.LightGreen;

                if (value <= 100)
                    ItemDataGridView.Rows[i].Cells[4].Style.BackColor = Color.LightYellow;

                if (value <= 50)
                    ItemDataGridView.Rows[i].Cells[4].Style.BackColor = Color.LightSalmon;

                if (value < 1)
                    ItemDataGridView.Rows[i].Cells[4].Style.BackColor = Color.OrangeRed;
            }
        }

        private void ItemDataGridView_Sorted(object sender, EventArgs e)
        {
            PopulateQuantityColors();
        }
    }
}
