﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace SAPLivestock2018.Forms
{
    public partial class StoreWindow : Form
    {
        Transaction transaction;

        public StoreWindow()
        {
            InitializeComponent();
            ItemDataGridViewBind();
        }

        private void StoreWindow_Load(object sender, EventArgs e)
        {
            ItemDataGridViewBind();
        }

        public void ItemDataGridViewBind()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            con.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Item", con);
            DataSet ds = new DataSet();

            da.Fill(ds, "Item");

            BindingSource bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            ItemDataGridView.DataSource = bs;
            con.Close();

            ItemDGBColorBind();
        }

        public void ItemDGBColorBind() {
            for (int i = 0; i < ItemDataGridView.RowCount; i++)
            {
                if ((int)ItemDataGridView.Rows[i].Cells[4].Value <= 100)
                    ItemDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightYellow;

                if ((int)ItemDataGridView.Rows[i].Cells[4].Value <= 50)
                    ItemDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.LightSalmon;

                if ((int)ItemDataGridView.Rows[i].Cells[4].Value < 1) {
                    ItemDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
                }
                    
            }
        }

        private void ItemSearchBox_TextChanged(object sender, EventArgs e)
        {
            BindingSource bs = new BindingSource();
            bs.DataSource = ItemDataGridView.DataSource;
            bs.Filter = ItemDataGridView.Columns[1].HeaderText.ToString() + " LIKE '%" + ItemSearchBox.Text + "%'";
            ItemDataGridView.DataSource = bs;
        }

        private void ItemDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            discountNUD.Value = 0;
            orderQuantityNUD.Value = 1;
            productTextBox.Text = ItemDataGridView.Rows[ItemDataGridView.SelectedRows[0].Index].Cells[1].Value.ToString();

            double price = Double.Parse(ItemDataGridView.Rows[ItemDataGridView.SelectedRows[0].Index].Cells[3].Value.ToString());
            priceTextBox.Text = price.ToString();

            ComputeTotalPricePerItem();
        }

        private void ComputeTotalPricePerItem()
        {
            double price = Double.Parse(ItemDataGridView.Rows[ItemDataGridView.SelectedRows[0].Index].Cells[3].Value.ToString());
            double totalprice = 0;
            double quantity = Int32.Parse(orderQuantityNUD.Value.ToString());

            totalprice = (price * Int32.Parse(orderQuantityNUD.Value.ToString()));

            if (discountNUD.Value > 0)
            {
                double discount = (double)discountNUD.Value;
                totalprice = totalprice - (totalprice * (discount * 0.01f));
            }

            totalprice = Math.Round(totalprice, 2);
            totalPriceTextBox.Text = totalprice.ToString();
        }

        private void ClearOrderInformation()
        {
            productTextBox.Text = "";
            priceTextBox.Text = "0";
            totalPriceTextBox.Text = "0";
            discountNUD.Value = 0;
            orderQuantityNUD.Value = 1;
        }

        private void orderQuantityNUD_ValueChanged(object sender, EventArgs e)
        {
            if (productTextBox.Text != "") 
                ComputeTotalPricePerItem();
        }

        private void AddOrderItemBtn_Click(object sender, EventArgs e)
        {
            // Name, Qty, Price, Discount
            if(productTextBox.Text == "")
            {
                MessageBox.Show("No item(s) to add!");
                return;
            }
            string name = productTextBox.Text;
            string qty = orderQuantityNUD.Value.ToString();
            string price = totalPriceTextBox.Text;
            string discount = discountNUD.Value.ToString();
            
            OrderListDataGridView.Rows.Add(name, qty, price, discount);
            ComputeAmountDue();
            ClearOrderInformation();

            if (OrderListDataGridView.Rows.Count > 1)
                checkoutBtn.Enabled = true;
        }

        private void ComputeAmountDue()
        {
            //MessageBox.Show("cell value changed");
            
            double sum = 0;
            for (int i = 0; i < OrderListDataGridView.Rows.Count; ++i)
            {
                sum += Convert.ToDouble(OrderListDataGridView.Rows[i].Cells[2].Value);
            }
            
            totalAmountDueTextBox.Text = sum.ToString();
        }

        private void discountNUD_ValueChanged(object sender, EventArgs e)
        {
            if (productTextBox.Text != "")
                ComputeTotalPricePerItem();
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            if(OrderListDataGridView.Rows.Count <= 1)
            {
                MessageBox.Show("No items yet!");
                return;
            }
            DialogResult dialogResult = MessageBox.Show("Clearing order list, are you sure?", "Clear Cart", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                ClearOrderList();
            }
            
        }
        
        public void ClearOrderList()
        {
            OrderListDataGridView.Rows.Clear();
            OrderListDataGridView.Refresh();
            totalAmountDueTextBox.Text = "";
        }

        private void OrderListDataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            //if click is on new row or header row
            if (e.RowIndex == OrderListDataGridView.NewRowIndex || e.RowIndex < 0)
                return;

            //Check if click is on specific column 
            if (e.ColumnIndex == OrderListDataGridView.Columns["action"].Index)
            {
                DialogResult dialogResult = MessageBox.Show("You are trying to delete an item from the list. Are you sure?", "Remove Item", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    foreach (DataGridViewRow item in this.OrderListDataGridView.SelectedRows)
                        OrderListDataGridView.Rows.RemoveAt(item.Index);

                    ComputeAmountDue();

                    if (OrderListDataGridView.Rows.Count <= 1)
                        checkoutBtn.Enabled = false;
                }
                else if (dialogResult == DialogResult.No)
                    return;
            }
        }
        

        private void checkoutBtn_Click(object sender, EventArgs e)
        {
            if (OrderListDataGridView.Rows.Count > 1)
            {
                MainWindow mainWindow = (MainWindow)this.ParentForm;
                try
                {
                    // Final Compute on amount due
                    ComputeAmountDue();
                    CollectOrderInformation();
                    checkoutBtn.Enabled = false;
                    mainWindow.InitiateCheckout(transaction);
                }
                catch (Exception)
                {
                    MessageBox.Show("Error: Could not find MainWindow!");
                    throw;
                }
            }
            else
            {
                MessageBox.Show("There are no item(s) on the list!");
                return;
            }
        }

        public void CollectOrderInformation()
        {
            transaction = new Transaction();
            List<string> orderItemNameList = new List<string>();
            List<int> orderItemQtyList = new List<int>();
            List<int> orderItemDiscList = new List<int>();
            List<double> orderItemPriceList = new List<double>();

            try
            {
                for(int i = 0; i <= OrderListDataGridView.Rows.Count -2; i++)
                {
                    string itemName = OrderListDataGridView.Rows[i].Cells[0].Value.ToString();
                    int itemQty = int.Parse(OrderListDataGridView.Rows[i].Cells[1].Value.ToString());
                    double itemPrice = double.Parse(OrderListDataGridView.Rows[i].Cells[2].Value.ToString());
                    int itemDisc = int.Parse(OrderListDataGridView.Rows[i].Cells[3].Value.ToString());
                    orderItemNameList.Add(itemName);
                    orderItemQtyList.Add(itemQty);
                    orderItemDiscList.Add(itemDisc);
                    orderItemPriceList.Add(itemPrice);
                }

                transaction.SetOrderItemNameList(orderItemNameList);
                transaction.SetOrderItemQtyList(orderItemQtyList);
                transaction.SetOrderItemDiscList(orderItemDiscList);
                transaction.SetOrderItemPriceList(orderItemPriceList);
                transaction.SetTotalAmountDue(double.Parse(totalAmountDueTextBox.Text));

                // important: keeps track of original total amount for further discount operation upon checkout
                transaction.SetOriginalTotalAmountDue();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }
        }

        private void ItemDataGridView_Sorted(object sender, EventArgs e)
        {
            ItemDGBColorBind();
        }

        private void productTextBox_TextChanged(object sender, EventArgs e)
        {
            AddOrderItemBtn.Enabled = productTextBox.Text == "" ? false : true;
        }
        
    }
}



//SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
//conn.Open();

/*
SqlCommand cmd = new SqlCommand();
cmd.CommandType = CommandType.Text;
cmd.Parameters.Add("itemName", SqlDbType.VarChar).Value = itemName;
cmd.CommandText = "SELECT item_name FROM dbo.Item WHERE item_name = @itemName";
*/


//conn.Close();