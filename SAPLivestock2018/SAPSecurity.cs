﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
using System.Configuration;

namespace SAPLivestock2018.Security
{

    public static class SAPSecurity
    {
        static bool isAdmin = false;
        static string password = "1234";

        private static string encryptKey = "IanHendrickTanSAPLivestock2018";
        

        public static void LogIn(string pass)
        {
            if (CheckPassword(pass))
                SetAdmin(true);
            else System.Windows.Forms.MessageBox.Show("Wrong Password!");
        }

        public static bool ChangePassword(string oldPass, string newPass)
        {
            if(CheckPassword(oldPass))
            {
                password = newPass;
                UpdatePasswordOnDB(newPass);
                return true;
            }

            else return false;
                
        }

        private static void UpdatePasswordOnDB(string value)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            SqlCommand cmd = null;

            string cmdString = "UPDATE [Security] SET Password = '" + Encryption.Encrypt(value,encryptKey) + "'";

            //Execution
            con.Open();
            cmd = new SqlCommand(cmdString, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        private static bool CheckPassword(string value)
        {
            
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPLivestock2018.Properties.Settings.SIIL2018ConnectionString"].ConnectionString);
            SqlCommand cmd = null;

            string cmdString = "SELECT TOP 1 Password FROM [Security]";

            //Execution
            con.Open();
            cmd = new SqlCommand(cmdString, con);
            string dbVal = (string)cmd.ExecuteScalar();
            con.Close();

            dbVal = Encryption.Decrypt(dbVal, encryptKey);

            return dbVal.Equals(value);
        }

        public static void LogOut()
        {
            SetAdmin(false);
        }

        public static bool IsAdmin()
        {
            return isAdmin;
        }

        public static void SetAdmin(bool value)
        {
            isAdmin = value;
        }
    }
}
