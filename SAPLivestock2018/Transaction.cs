﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SAPLivestock2018
{
    public class Transaction
    {
        private int customerID;
        private string customerName = "";
        private DateTime transactionDateTime;
        private double originalTotalAmountDue;
        private double totalAmountDue;
        private double amountPaying;
        private double change;
        private double cashDiscount;

        private List<string> OrderItemNameList;
        private List<int> OrderItemQtyList;
        private List<int> OrderItemDiscList;
        private List<double> OrderItemPriceList;

        public void SetCustomerID(int id)
        {
            customerID = id;
        }

        public int GetCustomerID()
        {
            return customerID;
        }

        public void SetCustomerName(string name)
        {
            customerName = name;
        }

        public string GetCustomerName()
        {
            return customerName;
        }

        public void SetTransactionDateTime(DateTime dateTime)
        {
            transactionDateTime = dateTime;
        }

        public DateTime GetTransactionDateTime()
        {
            return transactionDateTime;
        }

        public void SetTotalAmountDue(double amountDue)
        {
            totalAmountDue = amountDue;
            //originalTotalAmountDue = totalAmountDue;
        }

        public double GetTotalAmountDue()
        {
            return totalAmountDue;
        }

        public void SetAmountPaying(double payment)
        {
            amountPaying = payment;
        }

        public double GetAmountPaying()
        {
            return amountPaying;
        }

        private void ComputeChange()
        {
            change = amountPaying - totalAmountDue;
        }

        public double GetChange()
        {
            ComputeChange();
            return change;
        }

        public void SetOriginalTotalAmountDue()
        {
            originalTotalAmountDue = GetTotalAmountDue();
        }

        public void SetCashDiscount(double value)
        {
            totalAmountDue = originalTotalAmountDue;
            cashDiscount = value;
            totalAmountDue -= cashDiscount;
            ComputeChange();
        }

        public double GetCashDiscount()
        {
            return cashDiscount;
        }

        public void SetOrderItemNameList(List<string> nameList)
        {
            OrderItemNameList = nameList;
        }

        public List<string> GetOrderItemNameList()
        {
            return OrderItemNameList;
        }

        public void SetOrderItemQtyList(List<int> qtyList)
        {
            OrderItemQtyList = qtyList;
        }

        public List<int> GetOrderItemQtyList()
        {
            return OrderItemQtyList;
        }

        public void SetOrderItemDiscList(List<int> discount)
        {
            OrderItemDiscList = discount;
        }

        public List<int> GetOrderItemDiscList()
        {
            return OrderItemDiscList;
        }

        public void SetOrderItemPriceList(List<double> priceList)
        {
            OrderItemPriceList = priceList;
        }

        public List<double> GetOrderItemPriceList()
        {
            return OrderItemPriceList;
        }

        public DataTable GenerateDataTable()
        {
            DataTable dt = new DataTable();
            
            /*
            dt.Columns.AddRange(new DataColumn[]
            {
                new DataColumn ("qty", typeof(int)),
                new DataColumn ("name", typeof(string)),
                new DataColumn ("price", typeof(double)),
                new DataColumn ("discount", typeof(int))
            });
            */

            dt.Columns.Add("QTY", typeof(int));
            dt.Columns.Add("Item Name", typeof(string));
            dt.Columns.Add("Price", typeof(double));
            dt.Columns.Add("Discount (%)", typeof(int));

            for (int i = 0; i < OrderItemNameList.Count; i++)
            {
                dt.Rows.Add(OrderItemQtyList[i], OrderItemNameList[i],
                                OrderItemPriceList[i], OrderItemDiscList[i]);
            }
            return dt;
        }

    }
}
